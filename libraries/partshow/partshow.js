(function($){
    
    var partShowBlock = {
        options:{
            blockHeight: 140,
            textShow: Drupal.t('it4r_partshow_js_textshow'),
            textHide: Drupal.t('it4r_partshow_js_texthide'),
        },
        init: function(options, element){
            //записываем в переменную элемент
            var $current = element;
            $current.options = {};
            //объединяем дефолтные настройки и переданные
            $.extend($current.options, this.options, options);
            //заданная высота
            $current.options.adjustHeight = $current.options.blockHeight;
            //текущая высоту блока
            $current.options.realHeight = $current.outerHeight();
            //если текущая высота больше заданной для ограничения
            //инициализируем скрытие
            if ($current.options.realHeight > $current.options.adjustHeight) {
                //текст для раскрытия
                moreText = $current.options.textShow;
                //текст для скрытия
                lessText = $current.options.textHide;
                
                //оборачиваем в общий див блок и жобавляемые элементы
                $current.wrap('<div class="pr_partshow"></div>');
                
                //устанавливаем высоту и скрываем лишнее версткой
                $current.css('height', $current.options.adjustHeight).css('overflow', 'hidden').addClass('pr_partshow--block-closed');
                //добавляем параграф с троиточием и кнопку скрыть\показать
                $current.after('<span class="pr_partshow--continued">&hellip;</span><div class="pr_partshow--toggle"><span class="pr_partshow--toggle_link pr_partshow--toggle_link-closed"><span class="pr_partshow--toggle_link_inner">'+moreText+'</span></span></div>');

                //при нажатии на кнопку меняем класс и меняем css стили
                $current.nextAll('.pr_partshow--toggle').find('.pr_partshow--toggle_link').click(function (){
                    //меняем классы
                    $($current).toggleClass('pr_partshow--block-open').toggleClass('pr_partshow--block-closed');
                    $(this).toggleClass('pr_partshow--toggle_link-open').toggleClass('pr_partshow--toggle_link-closed');
                    //если текст нужно показывать
                    if ($($current).hasClass('pr_partshow--block-open')){
                        //меняем высоту на auto и css стили
                        $($current).css('height', 'auto').css('overflow', 'visible');
                        //скрываем троиточие
                        $(this).parent('.pr_partshow--toggle').prev('.pr_partshow--continued').hide();
                        //меняем текст в кнопке показать\скрыть на показать
                        $(this).find('.pr_partshow--toggle_link_inner').html(lessText);
                    }
                    //если нужно скрывать блок
                    if ($($current).hasClass('pr_partshow--block-closed')){
                        //меняем высоту на указаную пользователем
                        $($current).css('height', $current.options.adjustHeight).css('overflow', 'hidden');
                        //показываем троеточие
                        $(this).parent('.pr_partshow--toggle').prev('.pr_partshow--continued').show();
                        //меняем текст кнопке показать\скрыть на скрыть
                        $(this).find('.pr_partshow--toggle_link_inner').html(moreText);
                    }
                });
            }
        },
    }
    
    
    $.fn.partshow = function(options) {
        partShowBlock.init(options, this);
    };
  
})(jQuery);