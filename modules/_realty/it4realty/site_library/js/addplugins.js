(function($){
  
  var addPlugin = {
    config:{
      htmlElement: 'HtmlElement',
      initAttributes: 'AttributeInitialize',
      pluginName: 'PluginName'
    },
    init: function(config){
      //объеденяет содержимое первого объекта с последующими (что-то типа merge)
      $.extend(this.config, config);
      
      //получаем массив елементов к которым нужно будет добавить плагин
      //поиск ведем по HtmlElement[PluginName=AttributeInitialize]
      var pluginElementAdd = $(config.htmlElement+'['+config.initAttributes+'='+config.pluginName+']');
      
      //если такие элементы есть на страницы
      if(pluginElementAdd.length > 0){
	
		//перебераем каждый элемент и получаем опции для плагина
		$.each(pluginElementAdd, function(){
		  //создадим переменные для работы + закэшируем данные уже существующие данные
		  //что бы скрипт не ходил повторно по DOM
		  var $this = $(this),
			  at = this.attributes,
			  len = at.length,
			  pluginOption = {};

		  //перебираем все атрибуты найденных элементов
		  for (var i = 0; i < len; i++) {
			//есть в атрибуте есть data- и имя не равно data-plugin
			//запишем в объект опций плагина
			if ( (~at[i].name.indexOf('data-')) && (at[i].name !== 'data-plugin')){
			  pluginOption[$.camelCase(at[i].name.replace('data-',''))] = at[i].value;
			}
		  }

		  //быстрого умного решения пока не нашел
		  //оставим пока так 
		  if(config.pluginName == 'slider'){
            
			//сфорпируем данные для работы
			var sliderId = $this.attr('id')+"-slider",
			sliderMapValue = pluginOption.steps.split(", ");

			//добавили елемент в начало массива
			sliderMapValue.unshift("");

			pluginOption = {
			  animate: true,
			  max: sliderMapValue.length-1,
			  min: 0,
			  range: "min",
			  value: sliderMapValue.indexOf($this.val()),
			  //при изменении слайдера будем писать данные в текущий эелемент (input)
			  slide: function( event, ui ) {
                $this.val( sliderMapValue[ui.value] ); 
			  }
            };
            
			//на JS создаем елемент для слайдера и добавляем эго после текущего input
			//после чего подключил плагин slider и передадим опции для плагина
			$("<div id='"+sliderId+"'></div>").insertAfter($this)[config.pluginName](pluginOption);
            
            var sliderValue = 0;
            $this.keyup(function(){
              sliderValue = sliderMapValue.indexOf($this.val());
              if(sliderValue == -1){
                sliderMapValue.map(function(val,key){
                  var thisVal = Number($this.val().split(' ').join('')),
                      arrayVal = Number(val.split(' ').join(''));
                  if(thisVal > arrayVal){
                    sliderValue = key+1;
                  }
                });
              }
              $('#'+sliderId).slider("value", sliderValue);
            });
            
          }else{
            //вызавим наш плаги и передаем туда опции
            $this[config.pluginName](pluginOption);
          }

		});
	
      }
    }
  };
  
  addPlugin.init({
    htmlElement: 'select',
    initAttributes: 'data-plugin',
    pluginName: 'multiselect',
  });
  
  addPlugin.init({
    htmlElement: 'select',
    initAttributes: 'data-plugin',
    pluginName: 'selectmenu'
  });
  
  addPlugin.init({
    htmlElement: 'input',
    initAttributes: 'data-plugin',
    pluginName: 'slider'
  });
  
  addPlugin.init({
    htmlElement: 'form',
    initAttributes: 'data-plugin',
    pluginName: 'validate'
  });
  
  addPlugin.init({
    htmlElement: 'div',
    initAttributes: 'data-plugin',
    pluginName: 'partshow'
  });
  
//  jQuery(searchFormId + ' .visible-element :text').maskMoney({
//    'thousands': " ",
//    'precision': "",
//    'decimal': ""
//  });

})(jQuery);